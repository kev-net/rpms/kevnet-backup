Name:		kevnet-backup
Version:	%{ci_version}
Release:	%{ci_release}%{?dist}
Summary:	Backup scheduling tool for kevnet

License:	Apache-2.0
URL:		https://gitlab.com/kev-net/rpms/kevnet-backup/
Source0:	kevnet-backup
Source1:	kevnet-backup-prune
Source2:	kevnet-backup.conf
Source3:	10-kevnet.conf
Source4:	kevnet-backup-daily

BuildArch:	noarch
BuildRequires:	coreutils
Requires:	restic, crontabs, filesystem

%description
A backup scheduling tool for kevnet.  Backups are performed daily
using restic.


%package prune
Summary: Backup pruning tool for kevnet
Requires: restic, crontabs, filesystem, %{name}

%description prune
A backup pruning tool for kevnet.  Pruning occurs on a weekly basis,
as opposed to the daily basis for backing up.


%install
mkdir -p %{buildroot}/%{_sbindir}
cp -a %{SOURCE0} %{buildroot}/%{_sbindir}/kevnet-backup
mkdir -p %{buildroot}/%{_sysconfdir}/cron.daily
sed -s 's|@PATH@|%{_sbindir}|g' < %{SOURCE4} > %{buildroot}/%{_sysconfdir}/cron.daily/kevnet-backup
mkdir -p %{buildroot}/%{_sysconfdir}/cron.weekly
sed -s 's|@PATH@|%{_sbindir}|g' < %{SOURCE1} > %{buildroot}/%{_sysconfdir}/cron.weekly/kevnet-backup-prune
mkdir -p %{buildroot}/%{_sysconfdir}/sysconfig
cp -a %{SOURCE2} %{buildroot}/%{_sysconfdir}/sysconfig/kevnet-backup
mkdir -p %{buildroot}/%{_sysconfdir}/kevnet-backup
cp -a %{SOURCE3} %{buildroot}/%{_sysconfdir}/kevnet-backup/10-kevnet.conf


%files
%defattr(644,root,root)
%license LICENSE
%doc README.md
%attr(755,root,root) %{_sbindir}/kevnet-backup
%attr(755,root,root) %{_sysconfdir}/cron.daily/kevnet-backup
%config(noreplace) %{_sysconfdir}/sysconfig/kevnet-backup
%dir %attr(700,root,root) %{_sysconfdir}/kevnet-backup
%attr(644,root,root) %{_sysconfdir}/kevnet-backup/10-kevnet.conf

%files prune
%license LICENSE
%doc README.md
%attr(755,root,root) %{_sysconfdir}/cron.weekly/kevnet-backup-prune


%changelog
* Wed Feb 08 2023 Kevin L. Mitchell <klmitch@mit.edu>
- Initial creation of the RPM
